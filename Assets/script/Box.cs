using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;

public class Box : MonoBehaviour
{
    public float min_X = -5.0f, max_X = 5.0f;

    public bool canMove = false;
    public float move_speed;

    private Rigidbody myBody;

    public bool gameOver;
    public bool ignoreCollision;
    public bool ignoreTrigger;

    private void Awake()
    {
        myBody = GetComponent<Rigidbody>();
        myBody.useGravity = false;
    }

    // Start is called before the first frame update
    void Start()
    {
        canMove = true;
        if (Random.Range(0,2)>0)
        {
            move_speed *= -1f;
            
        }

        GameplayController.instance.currentBox = this;
    }

    // Update is called once per frame
    void Update()
    {
              MoveBox();
    }

    void MoveBox()
    {
        if (canMove)
        {
            Vector3 temp = transform.position;

            temp.x += move_speed * Time.deltaTime;

            if (temp.x > max_X )
            {
                move_speed *= -1f;
                
            }
            else if (temp.x < min_X)
            {
                move_speed *= -1f;
                
            }
            transform.position = temp;
            
        }
    }

    public  void DropBox()
    {
        canMove = false;
        myBody.useGravity = true;
    }
    
    void Landed()
    {
        if (gameOver)
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene("SceneOptions", LoadSceneMode.Additive);
            
           
            ignoreCollision = true;
            ignoreTrigger = true;

            GameplayController.instance.SpawnNewBox();
            GameplayController.instance.MoveCamera();    
            
             return;
        }
    }

    void RestartGame()
    {
        GameplayController.instance.RestartGame();
    }

    private void OnCollisionEnter(Collision target)
    {
        if (gameObject.tag == "Box")
        {
            if (target.gameObject.tag == "Platfrom")
            {
                Invoke("Landed",3f);
                ignoreCollision = true;
         
         
            }
        
            if (target.gameObject.tag == "NotBox")
            {
                Invoke("Landed",3f);
                ignoreCollision = true;
                transform.gameObject.tag = "NotBox";
            
            
                GameplayController.instance.SpawnNewBox();
             
            }

            if (ignoreCollision)
            {
                return;
                           
            }
        }

        
    }

    private void OnTriggerEnter(Collider target)
    {
        if (target.tag == "Gameover")
        {
              CancelInvoke("Landed");
              gameOver = true;
              ignoreTrigger = true;
              
              Invoke("RestartGame",2f);
        }
        
        if (ignoreTrigger)
        {
            return;
        }
    }
}
