using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boxspawner : MonoBehaviour
{
    public GameObject box_Prefab;

    // Start is called before the first frame update
   /* void Start()
    {
        SpawnBox();
    }    */ // ห้ามเปิด 

    // Update is called once per frame
    public  void SpawnBox()
    {
        GameObject box_Obj = Instantiate(box_Prefab);
        box_Obj.transform.position = transform.position;
    }

    
}
