using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameplayController : MonoBehaviour
{
    public static GameplayController instance;

    public Boxspawner Box_spawner;

    [HideInInspector]
    public Box currentBox;

    public CameraFollower cameraScript;
    private int moveCount;
    
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        Box_spawner.SpawnBox();
    }

    // Update is called once per frame
    void Update()
    {
        DetectInout();
    }

    void DetectInout()
    {
        if (Input.GetKeyDown("space"))
        {
            currentBox.DropBox();
        }
    }

    public void SpawnNewBox()
    {
          Invoke( "NewBox" , 3f);
    }

    void NewBox()
    {
        Box_spawner.SpawnBox();
    }

    public void MoveCamera()
    {
        moveCount++;
        if (moveCount == 3)
        {
            moveCount = 0;
            cameraScript.targetPos.y += 2;
        }
    }

    public void RestartGame()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene
            (UnityEngine.SceneManagement.SceneManager.GetActiveScene().name);
    }
    
    
    
    
    
}
